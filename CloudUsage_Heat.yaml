heat_template_version: "2021-04-16"
 
description: Template that creates a stack for CloudUsage project.
 
parameters:
  keypair:
    type: string
    description: nom de la keypair
  image_vm:
    type: string
    description: id de l'image de la vm
  flavor:
    type: string
    description: flavor de la vm

resources:
  cloud_usage_network:
    type: OS::Neutron::Net
    properties:
      admin_state_up: true
      name: cloudUsageNetwork
      
  cloud_usage_subnet:
    type: OS::Neutron::Subnet
    properties:
      network_id: { get_resource: cloud_usage_network }
      cidr: 192.168.1.0/24
 
  heat_router:
    type: OS::Neutron::Router
    properties:
      external_gateway_info: { network: External-ENSSAT }
      name: heat_router
 
  heat_router_interface:
    type: OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: heat_router }
      subnet: { get_resource: cloud_usage_subnet }
 
  ssh_secgroup:
    type: OS::Neutron::SecurityGroup
    properties:
      rules:
        - protocol: tcp
          remote_ip_prefix: 0.0.0.0/0
          port_range_min: 22
          port_range_max: 22

  web_secgroup:
    type: OS::Neutron::SecurityGroup
    properties:
      rules:
        - protocol: tcp
          remote_ip_prefix: 0.0.0.0/0
          port_range_min: 80
          port_range_max: 80
        - protocol: tcp
          remote_ip_prefix: 0.0.0.0/0
          port_range_min: 443
          port_range_max: 443

  mariadb_secgroup:
    type: OS::Neutron::SecurityGroup
    properties:
      rules:
        - protocol: tcp
          remote_ip_prefix: 0.0.0.0/0
          port_range_min: 3306
          port_range_max: 3306
 
  floating_ip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: External-ENSSAT
 
  association:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: floating_ip }
      port_id: { get_resource: vm1_port }
 
  vm1: 
    type: OS::Nova::Server 
    properties:
      key_name: { get_param: keypair }
      name: VM1
      image: { get_param: image_vm }
      flavor: { get_param: flavor }
      networks:
        - port: { get_resource: vm1_port }
      user_data_format: RAW
      user_data: |
        #cloud-config
        runcmd:
            - mkdir /run/dockercompose/
            - curl -o /run/dockercompose/docker-compose-apache.yml https://gitlab.enssat.fr/clegendre/cloudusage/-/raw/main/docker-compose-apache.yml
            - sudo docker-compose -f /run/dockercompose/docker-compose-apache.yml up -d

  vm1_port:
    type: OS::Neutron::Port
    properties:
      admin_state_up: true
      network: { get_resource: cloud_usage_network }
      security_groups:
        - { get_resource: web_secgroup }
        - { get_resource: ssh_secgroup }
      fixed_ips:
        - subnet_id: { get_resource: cloud_usage_subnet }

  vm2: 
    type: OS::Nova::Server 
    properties: 
      key_name: { get_param: keypair }
      name: VM2
      image: { get_param: image_vm }
      flavor: { get_param: flavor }
      networks:
        - port: { get_resource: vm2_port }
      user_data_format: RAW
      user_data: |
        #cloud-config
        runcmd:
            - PATH="/sbin:$PATH"
            - sudo mkfs.ext4 /dev/vdb
            - sudo mkdir -p /mnt/mariadb
            - sudo mount /dev/vdb /mnt/mariadb
            - sudo docker volume create --name mariadb_volume --opt type=none --opt device=/mnt/mariadb --opt o=bind
            - mkdir /run/dockercompose/
            - curl -o /run/dockercompose/docker-compose-mariadb.yml https://gitlab.enssat.fr/clegendre/cloudusage/-/raw/main/docker-compose-mariadb.yml
            - sudo docker-compose -f /run/dockercompose/docker-compose-mariadb.yml up -d
 
  vm2_port:
    type: OS::Neutron::Port
    properties:
      admin_state_up: true
      network: { get_resource: cloud_usage_network }
      security_groups:
        - { get_resource: ssh_secgroup }
        - { get_resource: mariadb_secgroup }

  mariadb_data:
    type: OS::Cinder::Volume
    properties:
      size: 2
  mariadb_data_att:
    type: OS::Cinder::VolumeAttachment
    properties:
      instance_uuid: { get_resource: vm2 }
      volume_id: { get_resource: mariadb_data}

outputs:
  floatingip:
    description: floating ip validation
    value: {get_attr: [floating_ip, floating_ip_address]}
