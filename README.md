Projet de CloudUsage de 3e année.

Loïc Conan, Clément Le Gendre

CloudUsage_Heat.yaml est le template qui permet de déployer la stack

CloudUsage_Heat_Env.yml est le fichier qui contient les variables d'environnement

docker-compose-apache.yml est le docker-compose qui est lancé automatiquement sur la VM1 pour créer le serveur web

docker-compose-mariadb.yml est le docker-compose qui est lancé automatiquement sur la VM2 pour créer le serveur mariadb
